var stockApp = angular.module('stockApp', ['ui.router', 'base64', 'ngDialog', 'ngCookies']);
stockApp
.config(function ($stateProvider, $urlRouterProvider,$locationProvider) {
    $locationProvider.html5Mode(false);
    $locationProvider.hashPrefix('!');
    $urlRouterProvider.otherwise('/');
    var states = [
        { name: 'home', url: '/', component: 'home' },
        { name: 'update', url: '/update', component: 'update' },
        { name: 'filter', url: '/filter', component: 'filter' },
        { name: 'login', url: '/login', component: 'login' },
        { name: 'register', url: '/register', component: 'register' },
        { 
            name: 'home.code',
            url: '{code}',
            component: 'stockSummary',
            resolve: {
                stock: function (StockService, $stateParams) {
                    return StockService.getStockByCode($stateParams.code)
                },
                price: function (StockService, $stateParams) {
                    return StockService.getPriceByStock($stateParams.code)
                }
            }
        },
        {
            name: 'filter.rec',
            url: '/rec?rec&avgScore',
            component: "stockRec",
            resolve: {
                recs: function (StockService, $stateParams) {
                    var params = {rec: $stateParams.rec, avgScore: $stateParams.avgScore}
                    return StockService.getStockByRecScore(params)
                }
            }
        }
    ];
    states.forEach(function(state) {
        $stateProvider.state(state);
    });

})
.run(function ($cookies, $rootScope) {
    $rootScope.isLogin = $cookies.get('isLogin') || false;
    console.log($rootScope.isLogin);
});
