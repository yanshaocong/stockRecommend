angular.module('stockApp').component('update', {
    templateUrl: 'templates/update.html',
    controller: function($scope, TokenService, AdminService) {
        this.isLogin = false;
        this.accessToken = "";
        this.submit = function() {
            console.log(this.accessToken)
            TokenService.postToken(this.token);
            console.log(TokenService.getToken());
        };

        this.update = function () {
        	console.log(TokenService.updateStockReview());
        };

        this.login = function () {
            var self = this;
            AdminService.login(this.user, this.password)
                .then(function (accessToken) {
                    self.accessToken = accessToken;
                    self.isLogin = true;
                }, function (argument) {
                    console.log(argument)
                });
        };
    }
});
