angular.module('stockApp').component('register', {
	templateUrl: 'templates/register.html',
	controller: function ($state, UserService, ngDialog) {
		this.isWarning = false;
		this.submit = function () {
			var self = this
			var data = {
				email: this.email,
				username: this.username,
				password: this.password,
				invite: this.invite
			};
			UserService.create(data).then(function(resp) {
				ngDialog.open({ template: 'templates/alert/createSuccess.html', className: 'ngdialog-theme-default', showClose: false});
			},function (err) {
				self.isWarning = true;
				switch (err.data.error) {
					case "username duplicate":
						self.warning = "用户名已被占用";
						break;
					case "invite code not right":
						self.warning = "邀请码错误";
						break;
				}
			})
		}
	}
})