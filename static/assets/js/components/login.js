angular.module('stockApp').component('login', {
	templateUrl: 'templates/login.html',
	controller: function ($state, UserService, $rootScope, $cookies) {
		this.login = function () {
			var data = {
				username: this.username,
				password: this.password
			};
			UserService.login(data).then(function(resp) {
				$rootScope.isLogin = true;
				var expireDate = new Date();
				expireDate.setDate(expireDate.getDate() + 15);
				$cookies.put('isLogin', true, {'expires': expireDate});
				$state.go("home");
			}, function (err) {
				$rootScope.isLogin = false;
				console.log(err);
			})
		};
	}
})