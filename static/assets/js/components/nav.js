angular.module('stockApp').controller('NavCtrl', function 
	($scope, $rootScope, ngDialog, $cookies) {
	$scope.logout = function () {
		$rootScope.isLogin = false;
		$cookies.remove('isLogin');
		ngDialog.open({ template: 'templates/alert/logoutAlert.html', className: 'ngdialog-theme-default'});
	}
});