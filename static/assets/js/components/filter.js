angular.module('stockApp').component('filter', {
	templateUrl: 'templates/filter.html',
	controller: function ($state) {
		this.rec = "Buy";
		this.avgScore = "10";
		this.search = function () {
			$state.go("filter.rec", {rec: this.rec, avgScore: this.avgScore});
			console.log(this.rec);
		}
	}
}).component('stockRec', {
	bindings: { recs: '<' },
	templateUrl:  'templates/stockRec.html',
	controller: function ($rootScope, ngDialog) {
		this.popup = false;
		this.sortByProp = "-Data.Summary.Meta.ANALYST_SCORE";
		this.sortBy = function (prop) {
			this.sortByProp = prop;
		};
		if (!$rootScope.isLogin) {
			setTimeout(function () {
				if (!this.popup) {
					ngDialog.open({ template: 'templates/alert/loginAlert.html', className: 'ngdialog-theme-default', showClose: false, closeByEscape: false, closeByDocument: false});
				}
			}, 2000);
		}
	}
});