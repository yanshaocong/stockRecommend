angular.module('stockApp').component('home', {
	transclude: true,
    templateUrl:  'templates/home.html',
    controller: function (StockService, $state, ngDialog) {
    	this.search = function() {
    		$state.go("home.code", {code: this.code});
    	}
    }
}).component('stockSummary', {
	bindings: { stock: '<', price: '<'},
	templateUrl:  'templates/stockSummary.html',
	controller: function ($rootScope, ngDialog) {
		this.popup = false;
		if (!$rootScope.isLogin) {
			setTimeout(function () {
				if (!this.popup) {
					ngDialog.open({ template: 'templates/alert/loginAlert.html', className: 'ngdialog-theme-default', showClose: false, closeByEscape: false, closeByDocument: false});
				}
			}, 2000);
		}
	}
});
