angular.module('stockApp').service('AdminService', function ($http, $base64) {
	var service = {
		login: function (user, pass) {
			var auth = $base64.encode(user + ":" + pass);
			var headers = {"Authorization": "Basic " + auth};
			return $http.get('secret', {headers: headers})
			.then(function (resp) {
				return resp.data;
			})
		}
	};
	return service;
});