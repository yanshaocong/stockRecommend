angular.module('stockApp').service('TokenService', function ($http) {
    var service = {
        getToken: function() {
            return $http.get('token', { cache: false }).then(function (resp) {
                return resp.data;
            });
        },
        postToken: function (token) {
            var config = {
                params: {
                    token: token
                }
            };
            return $http.post('token', null, config).then(function (resp) {
                return resp;
            });
        },
        updateStockReview: function () {
            return $http.get('update').then(function (resp) {
                return resp;
            })
        }
    };

    return service;
});
