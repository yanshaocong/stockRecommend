angular.module('stockApp').service('StockService', function ($http) {
	var service = {
		getStockByCode: function (code) {
			return $http.get('stock/' + code, {cache: true}).then(function(resp) {
				return resp.data;
			})
		},
		getStockByRecScore: function(data) {
			var params = JSON.stringify(data)
			return $http.post('rec', data).then(function (resp) {
				var data = resp.data.map(function (obj) {
					Object.keys(obj.Data.Summary.Meta).forEach(function (k) {

						if (k.match(/_SCORE(?!_)/)) {
							if (obj.Data.Summary.Meta[k].match("-")) {
								obj.Data.Summary.Meta[k] = -1
							} else {
								obj.Data.Summary.Meta[k] = parseInt(obj.Data.Summary.Meta[k], 10);

							}
						}
					});
					return obj;
				})
				return data;
			})
		},
		getPriceByStock: function (code) {

			return $http.get('price/' + code, {cache: false}).then(function (resp) {
				console.log(resp.data);
				return resp.data;
			})
		}
	};
	return service;
});