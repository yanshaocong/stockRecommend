angular.module('stockApp').service('UserService', function ($http) {
	var service = {
		login: function (data) {
			var params = JSON.stringify(data);
			return $http.post('user/login', params).then(function (resp) {
				return resp;
			})
		},
		create: function (data) {
			var params = JSON.stringify(data);
			return $http.post('user/create', params).then(function (resp) {
				return resp;
			})
		}
	};

	return service;
});