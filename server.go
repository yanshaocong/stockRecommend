package main

import (
	"github.com/iris-contrib/middleware/basicauth"
	"github.com/iris-contrib/middleware/recovery"
	"github.com/kataras/iris"
	"gitlab.com/yanshaocong/stockRecommend/api/price"
	"gitlab.com/yanshaocong/stockRecommend/api/stock"
	"gitlab.com/yanshaocong/stockRecommend/api/token"
	"gitlab.com/yanshaocong/stockRecommend/api/update"
	"gitlab.com/yanshaocong/stockRecommend/api/user"
)

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	token.CreateDB("token")
	authentication := basicauth.Default(map[string]string{"admin@admin.com": "opensesame"})
	iris.Config.IsDevelopment = true
	// iris.StaticCacheDuration = time.Duration(1)
	iris.Use(recovery.New())

	iris.Get("/", func(ctx *iris.Context) {
		ctx.Render("index.html", struct{ Name string }{Name: "iris"})
	})

	iris.Get("/secret", authentication, func(ctx *iris.Context) {
		ctx.Write("Success")
	})

	iris.Get("/update", update.DB)

	iris.Get("/token", token.GetToken)
	iris.Post("/token", token.SaveToken)

	iris.Get("/stock/:code", stock.GetStockByCode)
	iris.Get("/price/:code", price.GetPriceByStock)
	iris.Post("/rec", stock.GetStockByRecScore)
	iris.Post("/user/create", user.CreateUser)
	iris.Post("/user/login", user.LoginUser)

	iris.Static("/assets", "./static/assets/", 1)
	iris.Static("/css", "./static/assets/css", 1)
	iris.Static("/js", "./static/assets/js", 1)
	iris.Static("/lib", "./static/assets/lib", 1)
	iris.Static("/templates", "./templates", 1)
	iris.Listen(":3001")

}
