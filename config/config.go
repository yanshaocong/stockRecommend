package config

import "os"

type Config struct {
	DB struct {
		Name    string
		Address string
	}
	App struct {
		Invite string
	}
}

func GetConfig() Config {
	config := Config{}
	config.DB.Address = "mongodb://" + os.Getenv("DBADDR") + ":27017/"
	config.DB.Name = os.Getenv("DBNAME")
	config.App.Invite = "wechat2017"
	return config
}
