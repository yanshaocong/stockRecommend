package crawler

import (
	"sync"

	"github.com/robfig/cron"
)

type Crawler struct {
	ID       string
	stop     chan struct{}
	process  Process
	pipeline Pipeline
	wg       *sync.WaitGroup
}

type Process interface {
	Process() (*sync.WaitGroup, chan []byte)
}

type Pipeline interface {
	Pipeline(wg *sync.WaitGroup, res chan []byte)
}

func NewCrawler(p Process) *Crawler {
	ret := &Crawler{process: p}
	return ret
}

func (c *Crawler) AddPipeline(p Pipeline) *Crawler {
	ret := &Crawler{process: c.process, pipeline: p}
	return ret
}

func (c *Crawler) Run() {
	c.pipeline.Pipeline(c.process.Process())
}

func (c *Crawler) SimpleRun() {
	c.process.Process()
}

func (c *Crawler) ScheduleRun(timer string) {
	cron := cron.New()
	cron.AddFunc(timer, func() {
		c.pipeline.Pipeline(c.process.Process())
	})
	cron.Start()
}
