package equitySecurities

import (
	"encoding/json"
	"log"
	"sync"

	"github.com/PuerkitoBio/goquery"
)

type securitiesProcess struct {
	wg *sync.WaitGroup
}

type securitiesList struct {
	Code []string
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func NewSecuritiesProcess() *securitiesProcess {
	return &securitiesProcess{}
}

func (p *securitiesProcess) Process() (*sync.WaitGroup, chan []byte) {
	var sl securitiesList
	var wg sync.WaitGroup
	resultc := make(chan []byte)
	for i := 0; i < 1; i++ {
		wg.Add(1)
		go func() {
			urls := []string{"http://www.hkex.com.hk/chi/market/sec_tradinfo/stockcode/eisdeqty_c.htm", "http://www.hkex.com.hk/chi/market/sec_tradinfo/stockcode/eisdgems_c.htm"}
			for _, url := range urls {
				doc, err := goquery.NewDocument(url)
				checkError(err)
				doc.Find(".tr_normal").Each(func(i int, s *goquery.Selection) {
					sl.Code = append(sl.Code, s.Find("td").First().Text())
				})
			}
			res, _ := json.Marshal(sl)
			resultc <- res
		}()
	}
	return &wg, resultc
}
