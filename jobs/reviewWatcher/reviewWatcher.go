package reviewWatcher

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strings"
	"sync"
	"time"

	"github.com/boltdb/bolt"
	"golang.org/x/time/rate"
)

type stockSummaryProcess struct {
	token string
}

type stockSummary struct {
	Code string
	Data struct {
		Summary struct {
			Meta struct {
				TrendImgLink  string  `json:"TREND_IMG_LINK,omitempty"`
				Symbol        string  `json:"symbol,omitempty"`
				AvgScore      string  `json:"AVG_SCORE,omitempty"`         // 平均得分
				AnalystScore  string  `json:"ANALYST_SCORE,omitempty"`     //盈利
				RiskScore     string  `json:"RISK_SCORE,omitempty"`        //风险
				RvScore       string  `json:"RV_SCORE,omitempty"`          //相對估值
				FundScore     string  `json:"FUND_SCORE,omitempty"`        // 基本因素
				TechScore     string  `json:"TECH_SCORE,omitempty"`        // 價格動力
				AnnualDiv     float64 `json:"ANNUAL_DIV,string,omitempty"` // 末期股息
				AvgDailyVol   string  `json:"AVG_DAILY_VOL"`               // 每天交易量
				AvgScoreHl1   string  `json:"AVG_SCORE_HL1"`               //亮点
				AvgScoreHl2   string  `json:"AVG_SCORE_HL2"`               //亮点
				BusDesc       string  `json:"BUS_DESC"`
				CloseDate     string  `json:"CLOSE_DATE"`
				ClosePrice    string  `json:"CLOSE_PRICE"`
				DivYield      string  `json:"DIV_YIELD,omitempty"`   // 周息率
				ForwardPE     string  `json:"FORWARD_PE,omitempty"`  // 预测市盈率
				TrailingPE    string  `json:"TRAILING_PE,omitempty"` // 历史市盈率
				HighPrice52W  string  `json:"HIGHPRICE_52W,omitempty"`
				LowPrice52W   string  `json:"LOWPRICE_52W,omitempty"`
				MarketCap     string  `json:"MARKET_CAP"`
				name          string  `json:"nm"`
				RecText       string  `json:"REC_TEXT"`
				ReportDate    string  `json:"REPORT_DATE"`
				Revenue       string  `json:"REVENUE"`
				Roe           string  `json:"ROE,omitempty"`  // 股本回报率
				InstOwnership string  `json:"INST_OWNERSHIP"` // 机构持股
			}

			Peer interface{}
		}

		Concensus struct {
			Actual   interface{} `json:"actual"`
			Estimate interface{} `json:"estimate"`
		}
	}
	Timestamp time.Time `bson:"timestamp"`
}

type stockPeer struct {
	Data struct {
		Summary struct {
			Peer interface{} `json:"peer"`
		}
	}
}

type securitiesList struct {
	Code []string
}

func NewStockSummaryProcess(token string) *stockSummaryProcess {
	return &stockSummaryProcess{token}
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func checkCodeError(cd string, err error) {
	if err != nil {
		fmt.Print(cd)
	}
}

func getStockList() <-chan string {
	var list securitiesList
	out := make(chan string)
	db, err := bolt.Open("bolt.db", 0600, nil)
	checkError(err)
	defer db.Close()
	db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("equity"))
		c := b.Cursor()
		_, v := c.Last()
		err := json.Unmarshal(v, &list)
		checkError(err)
		return nil
	})
	if len(list.Code) > 0 {
		ratePerMin := float64(20)
		intervalMsec := time.Duration((1/ratePerMin)*60*1000) * time.Millisecond
		limitTime := rate.Every(intervalMsec)
		limit := rate.NewLimiter(limitTime, 100*1024)
		count := 0
		go func() {
			for _, n := range list.Code {
				rv := limit.Reserve()
				if !rv.OK() {
					return
				}
				time.Sleep(rv.Delay())
				count++
				fmt.Println("===== ", count, " / ", len(list.Code), " =====")
				out <- n
			}
			close(out)
		}()
	}
	return out
}

func (p *stockSummaryProcess) Process() (*sync.WaitGroup, chan []byte) {

	var wg sync.WaitGroup
	resultc := make(chan []byte)
	list := getStockList()

	for i := 0; i < 5; i++ {
		wg.Add(1)
		go func() {
			for cd := range list {
				var st stockSummary
				var sp stockPeer
				token := p.token
				base := "https://www2.trkd-hs.com/citibankwidget/data/srplus?&exch=HK&lang=zh_HK&id=1&region=HKGCB&channel=CBOL"
				url1 := base + "&estimate=1" + "&symbol=" + cd + "&token=" + token
				url2 := base + "&symbol=" + cd + "&token=" + token
				st.Code = cd
				st.Timestamp = time.Now()
				fmt.Println(st.Code)
				resp, err := http.Get(url1)
				checkCodeError(cd, err)
				if resp != nil {
					body, _ := ioutil.ReadAll(resp.Body)
					matched, _ := regexp.MatchString("Invalid", string(body))

					if !matched {
						dec := json.NewDecoder(strings.NewReader(string(body)))
						for {
							if err := dec.Decode(&st); err == io.EOF {
								break
							} else {
								checkError(err)
							}
						}

						resp, err = http.Get(url2)
						checkCodeError(cd, err)
						if resp != nil {
							body, _ = ioutil.ReadAll(resp.Body)
							dec = json.NewDecoder(strings.NewReader(string(body)))
							for {
								if err := dec.Decode(&sp); err == io.EOF {
									st.Data.Summary.Peer = sp.Data.Summary.Peer
									break
								} else {
									checkCodeError(cd, err)
								}
							}
						}

						r, err := json.Marshal(st)
						checkCodeError(cd, err)
						resultc <- r
					} else {
						r, err := json.Marshal(nil)
						checkCodeError(cd, err)
						resultc <- r
					}

				} else {
					r, err := json.Marshal(nil)
					checkCodeError(cd, err)
					resultc <- r
				}

			}
			wg.Done()
		}()
	}

	return &wg, resultc
}
