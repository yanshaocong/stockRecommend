package boltPipeline

import (
	"log"
	"sync"
	"time"

	"github.com/boltdb/bolt"
)

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

type boltPipeline struct {
	DbName string
}

func NewBoltPipeline(dbName string) *boltPipeline {
	db, err := bolt.Open("bolt.db", 0600, &bolt.Options{Timeout: 10 * time.Second})
	checkError(err)
	db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(dbName))
		checkError(err)
		return err
	})
	db.Close()
	return &boltPipeline{DbName: dbName}
}

func (p *boltPipeline) Pipeline(wg *sync.WaitGroup, res chan []byte) {
	go func() {
		for r := range res {
			db, err := bolt.Open("bolt.db", 0600, &bolt.Options{Timeout: 10 * time.Second})
			checkError(err)
			db.Update(func(tx *bolt.Tx) error {
				b := tx.Bucket([]byte(p.DbName))
				t := time.Now().Format(time.RFC3339)
				err := b.Put([]byte(t), r)
				return err
			})
			db.Close()
			wg.Done()
		}
	}()
	wg.Wait()
}
