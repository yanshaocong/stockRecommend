package mongoPipeline

import (
	"encoding/json"
	"io"
	"log"
	"strings"
	"sync"

	"gopkg.in/mgo.v2"
)

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

type MongoPipeline struct {
	Address        string
	DbName         string
	CollectionName string
}

func NewMongoPipeline(Address string, DbName string, CollectionName string) *MongoPipeline {
	return &MongoPipeline{Address, DbName, CollectionName}
}

func (p *MongoPipeline) Pipeline(wg *sync.WaitGroup, res chan []byte) {
	go func() {
		for r := range res {
			var model map[string]interface{}
			session, err := mgo.Dial(p.Address)
			checkError(err)

			c := session.DB(p.DbName).C(p.CollectionName)
			if string(r) != "null" {
				dec := json.NewDecoder(strings.NewReader(string(r)))
				for {
					if err := dec.Decode(&model); err == io.EOF {
						break
					} else {
						checkError(err)
					}
				}
				err = c.Insert(model)
				checkError(err)
			}

			session.Close()
		}

	}()
	wg.Wait()
}
