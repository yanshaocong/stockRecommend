package stock

import (
	"regexp"
	"time"

	"github.com/kataras/iris"
	"gitlab.com/yanshaocong/stockRecommend/config"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type recTime struct {
	Timestamp string `bson:"Timestamp"`
}

type RecForm struct {
	Rec      string
	AvgScore string
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func GetStockByCode(ctx *iris.Context) {
	var result interface{}
	conf := config.GetConfig()
	dbAddress := conf.DB.Address
	dbName := conf.DB.Name
	code := ctx.Param("code")
	session, err := mgo.Dial(dbAddress)
	checkError(err)
	c := session.DB(dbName).C("review")

	err = c.Find(bson.M{"Code": code}).Sort("-Timestamp").One(&result)

	checkError(err)
	ctx.JSON(iris.StatusOK, result)
}

func GetStockByRecScore(ctx *iris.Context) {
	var lastRes recTime
	var res []interface{}
	conf := config.GetConfig()
	dbAddress := conf.DB.Address
	dbName := conf.DB.Name
	var recForm RecForm
	if err := ctx.ReadJSON(&recForm); err != nil {
		panic(err.Error())
	}
	session, err := mgo.Dial(dbAddress)
	checkError(err)
	c := session.DB(dbName).C("review")

	err = c.Find(bson.M{
		"Data.Summary.Meta.REC_TEXT":  recForm.Rec,
		"Data.Summary.Meta.AVG_SCORE": recForm.AvgScore,
	}).Select(bson.M{"Timestamp": 1}).Sort("-Timestamp").One(&lastRes)
	checkError(err)

	re := regexp.MustCompile("-..:..")
	lastRes.Timestamp = re.ReplaceAllString(lastRes.Timestamp, "")
	date, _ := time.Parse("2006-01-02T15:04:05.000000000", lastRes.Timestamp)
	year, month, day := date.Date()
	searchTime := time.Date(year, month, day, 0, 0, 0, 0, date.Location()).Format("2006-01-02T15:04:05.000000000+08:00")
	err = c.Find(bson.M{
		"Data.Summary.Meta.REC_TEXT":  recForm.Rec,
		"Data.Summary.Meta.AVG_SCORE": recForm.AvgScore,
		"Timestamp": bson.M{
			"$gte": searchTime,
		},
	}).Select(bson.M{"Code": 1, "Data.Summary.Meta": 1}).Sort("-Timestamp").All(&res)

	checkError(err)
	ctx.JSON(iris.StatusOK, res)
}
