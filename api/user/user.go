package user

import (
	"fmt"
	"github.com/kataras/iris"
	"gitlab.com/yanshaocong/stockRecommend/config"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

type stockUser struct {
	Email    string
	Username string
	Password string
	Invite   string
}

func CreateUser(ctx *iris.Context) {
	var user, res stockUser

	if err := ctx.ReadJSON(&user); err != nil {
		panic(err.Error())
	}
	conf := config.GetConfig()
	dbAddress := conf.DB.Address
	dbName := conf.DB.Name
	invite := conf.App.Invite
	if user.Invite == invite {
		session, err := mgo.Dial(dbAddress)
		checkError(err)
		c := session.DB(dbName).C("stockUser")
		c.Find(bson.M{"username": user.Username}).One(&res)
		if res.Username != "" {
			ctx.JSON(iris.StatusUnauthorized, iris.Map{
				"error": "username duplicate",
			})
		} else {
			err = c.Insert(user)
			checkError(err)
			ctx.JSON(iris.StatusOK, "success")
		}
	} else {
		ctx.JSON(iris.StatusUnauthorized, iris.Map{
			"error": "invite code not right",
		})
	}
}

func LoginUser(ctx *iris.Context) {
	var user, res stockUser
	if err := ctx.ReadJSON(&user); err != nil {
		panic(err.Error())
	}
	fmt.Print(user, user.Username)
	conf := config.GetConfig()
	dbAddress := conf.DB.Address
	dbName := conf.DB.Name
	if user.Username != "" {
		password := user.Password
		session, err := mgo.Dial(dbAddress)
		checkError(err)
		c := session.DB(dbName).C("stockUser")

		c.Find(bson.M{"username": user.Username}).One(&res)

		if res.Password == password {
			ctx.Write("Success")
		} else {
			ctx.JSON(iris.StatusUnauthorized, iris.Map{
				"error": "password not right",
			})
		}
	} else {
		ctx.JSON(iris.StatusUnauthorized, iris.Map{
			"error": "you don't have access",
		})
	}
}
