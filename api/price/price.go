package price

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/kataras/iris"
)

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

type StockPrice struct {
	Code      string
	Time      string  `json:"ltt"`
	NowPrice  float64 `json:"np,string"`
	LastPrice float64 `json:"ltp,string"`
	Vollum    int     `json:"vol,string"`
	Turnover  int     `json:"tvr,string"`
	DayHigh   float64 `json:"dyh,string"`
	DayLow    float64 `json:"dyl,string"`
}

func getStockPrice(code string, date int64) StockPrice {
	var stockPrice StockPrice
	url := "http://money18.on.cc/js/real/hk/quote/" + code + "_r.js?t=" + strconv.FormatInt(date, 10)
	resp, err := http.Get(url)
	checkError(err)
	body, _ := ioutil.ReadAll(resp.Body)
	matched, err := regexp.MatchString(string(body), "<")
	if !matched {
		reg := regexp.MustCompile(".*=")
		decodeBody := reg.ReplaceAllString(string(body), "")
		reg1 := regexp.MustCompile(";")
		decodeBody = reg1.ReplaceAllString(decodeBody, "")
		reg2 := regexp.MustCompile("'")
		decodeBody = reg2.ReplaceAllString(decodeBody, "\"")
		dec := json.NewDecoder(strings.NewReader(decodeBody))
		for {
			var s StockPrice
			if err := dec.Decode(&s); err == io.EOF {
				break
			} else {
				checkError(err)
			}
			s.Code = code
			stockPrice = s
		}
	}
	return stockPrice
}

func GetPriceByStock(ctx *iris.Context) {
	code := ctx.Param("code")
	sp := getStockPrice(code, time.Now().Unix())
	ctx.JSON(iris.StatusOK, sp)
}
