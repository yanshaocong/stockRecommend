package token

import (
	"time"

	"github.com/boltdb/bolt"
	"github.com/kataras/iris"
)

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func CreateDB(dbName string) {
	db, err := bolt.Open("bolt.db", 0600, &bolt.Options{Timeout: 10 * time.Second})
	checkError(err)
	db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(dbName))
		checkError(err)
		return err
	})
	db.Close()
}

func SaveToken(ctx *iris.Context) {
	token := ctx.FormValue("token")
	db, err := bolt.Open("bolt.db", 600, &bolt.Options{Timeout: 1 * time.Second})
	checkError(err)
	defer db.Close()

	db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("token"))
		t := time.Now().Format(time.RFC3339)
		err := b.Put([]byte(t), token)
		ctx.Write("Token has been saved", token)
		return err
	})
}

func GetToken(ctx *iris.Context) {
	db, err := bolt.Open("bolt.db", 600, &bolt.Options{Timeout: 1 * time.Second})
	checkError(err)
	defer db.Close()

	db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("token"))
		c := b.Cursor()

		k, v := c.Last()
		ctx.Write("Last Token %s, Token is %s", k, string(v))
		return nil
	})
}
