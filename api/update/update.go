package update

import (
	"time"

	"github.com/boltdb/bolt"
	"github.com/kataras/iris"
	"gitlab.com/yanshaocong/stockRecommend/config"
	"gitlab.com/yanshaocong/stockRecommend/crawler"
	"gitlab.com/yanshaocong/stockRecommend/jobs/equitySecurities"
	"gitlab.com/yanshaocong/stockRecommend/jobs/reviewWatcher"
	"gitlab.com/yanshaocong/stockRecommend/pipeline/boltPipeline"
	"gitlab.com/yanshaocong/stockRecommend/pipeline/mongoPipeline"
)

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func DB(ctx *iris.Context) {
	var token string
	conf := config.GetConfig()
	dbAddress := conf.DB.Address
	dbName := conf.DB.Name

	db, err := bolt.Open("bolt.db", 600, &bolt.Options{Timeout: 1 * time.Second})
	checkError(err)

	db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("token"))
		c := b.Cursor()

		_, v := c.Last()
		token = string(v)
		return nil
	})
	db.Close()

	init1 := crawler.NewCrawler(equitySecurities.NewSecuritiesProcess()).
		AddPipeline(boltPipeline.NewBoltPipeline("equity"))
	init1.Run()
	init := crawler.NewCrawler(reviewWatcher.NewStockSummaryProcess(token)).
		AddPipeline(mongoPipeline.NewMongoPipeline(dbAddress, dbName, "review"))
	init.Run()

	ctx.Write("Complete")
}
